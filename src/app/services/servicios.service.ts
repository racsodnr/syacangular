import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {  

  constructor(private http: HttpClient) { }

  listarClientes() {
    return this.http.get('');
  }

  listarPedidos() {
    return this.http.get('');
  }
}
