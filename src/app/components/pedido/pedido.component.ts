import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faSearch, faTrash, faEdit, faPlus, faSave } from '@fortawesome/free-solid-svg-icons';
import { ServiciosService } from '../../services/servicios.service';
import { PedidoModel } from '../../models/pedido.model';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {
  faSearch = faSearch;
  faTrash = faTrash;
  faEdit = faEdit;
  faPlus = faPlus;
  faSave = faSave;

  fechaActual: Date;

  pedido: PedidoModel = new PedidoModel;

  forma: FormGroup

  constructor(
    private fb: FormBuilder,
    private servicios: ServiciosService
  ) {
    this.fechaActual = new Date();
    this.crearFormulario();
  }

  ngOnInit(): void {

  }

  cargarClientes() {
    this.servicios.listarClientes()
      .subscribe(resp => {
        console.log(resp);

      });
  }

  crearFormulario() {

    this.forma = this.fb.group({
      idPedido: [this.pedido.idPedido],
      idCliente: [this.pedido.idCliente, Validators.required],
      nombreCliente: [this.pedido.nombreCliente, Validators.required],
      fechaRegistro: [this.pedido.fechaRegistro, Validators.required],
      direccionEntrega: [this.pedido.direccionEntrega, Validators.required],
      estado: [this.pedido.estado],
      valorTotal: [this.pedido.valorTotal],
      productos: this.fb.array([])
    })

  }

  get productos() {
    return this.forma.get('productos') as FormArray;
  }

  get clienteNoValido() {
    return this.forma.get('idCliente').invalid && this.forma.get('idCliente').touched;
  }

  get direccionNoValida() {
    return this.forma.get('direccionEntrega').invalid && this.forma.get('direccionEntrega').touched;
  }

  guardar() {

    if (this.forma.invalid) {

      Object.values(this.forma.controls).forEach(control => {
        control.markAllAsTouched();
      })

      return;
    }

  }

  agregarProducto() {
    this.productos.push(this.fb.control('', Validators.required));
  }

  quitarProducto(item: number) {
    this.productos.removeAt(item);
  }

}
