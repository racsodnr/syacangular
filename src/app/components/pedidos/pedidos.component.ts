import { Component, OnInit } from '@angular/core';
import { faEdit, faPlus } from '@fortawesome/free-solid-svg-icons';
import { ServiciosService } from '../../services/servicios.service';

@Component({
  selector: 'app-pedidos',
  templateUrl: './pedidos.component.html',
  styleUrls: ['./pedidos.component.css']
})
export class PedidosComponent implements OnInit {

  faEdit = faEdit;
  faPlus = faPlus;

  pedidos: {};

  constructor(private servicios: ServiciosService) { }

  ngOnInit(): void {
  }

  cargarPedidos() {
    this.servicios.listarPedidos().subscribe(resp => {
      console.log(resp);
      this.pedidos = resp;

    })
  }

}
