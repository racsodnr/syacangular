import { Component, OnInit } from '@angular/core';
import { faEdit, faPlus } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  faEdit = faEdit;
  faPlus = faPlus;

  constructor() { }

  ngOnInit(): void {
  }

}
