export class PedidoModel {
    idPedido: string;
    idCliente: string;
    nombreCliente: string;
    fechaRegistro: Date;
    direccionEntrega: string;
    estado: string;
    valorTotal: number;

    constructor(){
        this.fechaRegistro = new Date();
        this.estado = ""
        this.valorTotal = 0;
    }
}
